function [tz ,r31 ,r32 ,r33 ,m11 ,m12 ,m13 ,m14 ,m21 ,m22 ,m23 ,m24 ,alfax,alfay,U0,V0,ec] = CalibraUmaCamera(Xs1 ,Ys1,u1,v1,...
   Xs2 ,Ys2,u2,v2,...
   Xs3 ,Ys3,u3,v3,...
   Xs4 ,Ys4,u4,v4,...
   Xs5 ,Ys5,u5,v5,...
   Xs6 ,Ys6,u6,v6,...
   Xs7 ,Ys7,u7,v7,...
   Xs8 ,Ys8,u8,v8,...
   Xs9 ,Ys9,u9,v9,...
   Xs10 ,Ys10,u10,v10,...
   Xs11,Ys11,u11,v11,...
   Xs12 ,Ys12,u12,v12,...
   Xs13,Ys13,u13,v13,...
   Xs14 ,Ys14,u14,v14,...
   Xs15 ,Ys15,u15,v15,...
   Xs16 ,Ys16,u16,v16,...
   Xs17 ,Ys17,u17,v17,...
   Xs18 ,Ys18,u18,v18,...
   Zs1,Zs2,Zs3,Zs4,Zs5,...
   Zs6,Zs7,Zs8,Zs9,Zs10,...
   Zs11,Zs12,Zs13,Zs14,Zs15,...
   Zs16,Zs17,Zs18...
   )

A = [u1*Xs1 u1*Ys1 u1*Zs1 -Xs1 -Ys1 -Zs1 -1 0 0 0 0;
    v1*Xs1 v1*Ys1 v1*Zs1 0 0 0 0 -Xs1 -Ys1 -Zs1 -1;
    u2*Xs2 u2*Ys1 u2*Zs2 -Xs2 -Ys2 -Zs2 -1 0 0 0 0;
    v2*Xs2 v2*Ys1 v2*Zs2 0 0 0 0 -Xs2 -Ys2 -Zs2 -1;
    u3*Xs3 u3*Ys1 u3*Zs3 -Xs3 -Ys3 -Zs3 -1 0 0 0 0;
    v3*Xs3 v3*Ys1 v3*Zs3 0 0 0 0 -Xs3 -Ys3 -Zs3 -1;
    u4*Xs4 u4*Ys1 u4*Zs4 -Xs4 -Ys4 -Zs4 -1 0 0 0 0;
    v4*Xs4 v4*Ys1 v4*Zs4 0 0 0 0 -Xs4 -Ys4 -Zs4 -1;
    u5*Xs5 u5*Ys1 u5*Zs5 -Xs5 -Ys5 -Zs5 -1 0 0 0 0;
    v5*Xs5 v5*Ys1 v5*Zs5 0 0 0 0 -Xs5 -Ys5 -Zs5 -1;
    u6*Xs6 u6*Ys1 u6*Zs6 -Xs6 -Ys6 -Zs6 -1 0 0 0 0;
    v6*Xs6 v6*Ys1 v6*Zs6 0 0 0 0 -Xs6 -Ys6 -Zs6 -1;
    u7*Xs7 u7*Ys1 u7*Zs7 -Xs7 -Ys7 -Zs7 -1 0 0 0 0;
    v7*Xs7 v7*Ys1 v7*Zs7 0 0 0 0 -Xs7 -Ys7 -Zs7 -1;
    u8*Xs8 u8*Ys1 u8*Zs8 -Xs8 -Ys8 -Zs8 -1 0 0 0 0;
    v8*Xs8 v8*Ys1 v8*Zs8 0 0 0 0 -Xs8 -Ys8 -Zs8 -1;
    u9*Xs9 u9*Ys1 u9*Zs9 -Xs9 -Ys9 -Zs9 -1 0 0 0 0;
    v9*Xs9 v9*Ys1 v9*Zs9 0 0 0 0 -Xs9 -Ys9 -Zs9 -1;
    u10*Xs10 u10*Ys10 u10*Zs10 -Xs10 -Ys10 -Zs10 -1 0 0 0 0;
    v10*Xs10 v10*Ys10 v10*Zs10 0 0 0 0 -Xs10 -Ys10 -Zs10 -1;
    u11*Xs11 u11*Ys11 u11*Zs11 -Xs11 -Ys11 -Zs11 -1 0 0 0 0;
    v11*Xs11 v11*Ys11 v11*Zs11 0 0 0 0 -Xs11 -Ys11 -Zs11 -1;
    u12*Xs12 u12*Ys12 u12*Zs12 -Xs12 -Ys12 -Zs12 -1 0 0 0 0;
    v12*Xs12 v12*Ys12 v12*Zs12 0 0 0 0 -Xs12 -Ys12 -Zs12 -1;
    ];

B = -[u1; v1; u2; v2; u3; v3; u4; v4; u5; v5; u6; v6; u7; v7; u8; v8; u9; v9; u10; v10; u11; v11; u12; v12];

V = pinv(A)*B;





%% separacao das variaveis
tz = 1/(sqrt(V(1)*V(1)+ V(2)*V(2)+V(3)*V(3)));

r31 = V(1)*tz %Sr31SobreTz*tz;
r32 = V(2)*tz%Sr32SobreTz*tz;
r33 = V(3)*tz%Sr33SobreTz*tz;
m11 = V(4)*tz%Sm11SobreTz*tz;
m12 = V(5)*tz%Sm12SobreTz*tz;
m13 = V(6)*tz%Sm13SobreTz*tz;
m14 = V(7)*tz%Sm14SobreTz*tz;
m21 = V(8)*tz%Sm21SobreTz*tz;
m22 = V(9)*tz%Sm22SobreTz*tz;
m23 = V(10)*tz%Sm23SobreTz*tz;
m24 = V(11)*tz%Sm24SobreTz*tz;

%% verificacao
ec = 0;

aux = [m11 m12 m13 m14; m21 m22 m23 m24; r31 r32 r33 tz]*[Xs13; Ys13; Zs13; 1];
u13_calc = aux(1)/aux(3);
v13_calc = aux(2)/aux(3);

aux = [m11 m12 m13 m14; m21 m22 m23 m24; r31 r32 r33 tz]*[Xs14; Ys14; Zs14; 1];
u14_calc = aux(1)/aux(3);
v14_calc = aux(2)/aux(3);

aux = [m11 m12 m13 m14; m21 m22 m23 m24; r31 r32 r33 tz]*[Xs15; Ys15; Zs15; 1];
u15_calc = aux(1)/aux(3);
v15_calc = aux(2)/aux(3);

aux = [m11 m12 m13 m14; m21 m22 m23 m24; r31 r32 r33 tz]*[Xs16; Ys16; Zs16; 1];
u16_calc = aux(1)/aux(3);
v16_calc = aux(2)/aux(3);

aux = [m11 m12 m13 m14; m21 m22 m23 m24; r31 r32 r33 tz]*[Xs17; Ys17; Zs17; 1];
u17_calc = aux(1)/aux(3);
v17_calc = aux(2)/aux(3);

aux = [m11 m12 m13 m14; m21 m22 m23 m24; r31 r32 r33 tz]*[Xs18; Ys18; Zs18; 1];
u18_calc = aux(1)/aux(3);
v18_calc = aux(2)/aux(3);

e13 = sqrt((u13-u13_calc)*(u13-u13_calc)+(v13-v13_calc)*(v13-v13_calc));
e14 = sqrt((u14-u14_calc)*(u14-u14_calc)+(v14-v14_calc)*(v14-v14_calc));
e15 = sqrt((u15-u15_calc)*(u15-u15_calc)+(v15-v15_calc)*(v15-v15_calc));
e16 = sqrt((u16-u16_calc)*(u16-u16_calc)+(v16-v16_calc)*(v16-v16_calc));
e17 = sqrt((u17-u17_calc)*(u17-u17_calc)+(v17-v17_calc)*(v17-v17_calc));
e18 = sqrt((u18-u18_calc)*(u18-u18_calc)+(v18-v18_calc)*(v18-v18_calc));

ec = e13 + e14 + e15 + e16 + e17 + e18;

% syms Cnx Cny Ctx Cty Cbx Cby 
% syms alfaxx alfayy u0 v0 Xom Yom

% Q = [0 r32 0 -r31 -1 0;...
%     -r32 0 r31 0 0 -1;...
%     -1 0 0 r33 0 -r32;...
%     0 -1 -r33 0 r32 0;
%     0 -r33 -1 0 0 r31;
%     r33 0 0 -1 -r31 0];
% W =[0;
%     
% [Cnx,Cny,Ctx, Cty, Cbx, Cby] = solve(...
%     Cny*r32 - r31*Cty == Cbx,...
%     r31*Ctx - Cnx*r32 == Cby,...
%     %Cnx*Cty - Cny*Ctx == r33,...
%     Cty*r33 - r32*Cby == Cnx,...
%     r32*Cbx - Ctx*r33 == Cny,...
%     %Ctx*Cby - Cty*Cbx == r31...
%      Cby*r31 - r33*Cny == Ctx,...
%      r33*Cnx - Cbx*r31 == Cty,...
% %     Cbx*Cny - Cby*Cnx == r32...

%     1==Cnx*Cnx + Cny*Cny +r31*r31,...
%     1==Ctx*Ctx + Cty*Cty +r32*r32,...
%     1==Cbx*Cbx + Cby*Cby +r33*r33,...
%     1==Cnx*Cnx + Ctx*Ctx +Cbx*Cbx,...
%     1==Cny*Cny + Cty*Cty +Cby*Cby,...

 vetorCOS= fsolve(@SistemaCOS,[-5 -5 -5 -5 -5 -5],[],r31,r32,r33);
Cnx = vetorCOS(1)
Cny = vetorCOS(2);
Ctx = vetorCOS(3)
Cty = vetorCOS(4);
Cbx = vetorCOS(5)
Cby = vetorCOS(6);
    
% [Salfaxx ,Salfayy ,Su0 ,Sv0 ,SXom ,SYom] = solve(...
%     m11==alfaxx*Cnx + u0*r31,...
%     m21==alfayy*Cny + v0*r31,...
%     m12==alfaxx*Ctx + u0*r32,...
%     m22==alfayy*Cty + v0*r32,...
%     m13==alfaxx*Cbx + u0*r33,...
%     m23==alfayy*Cby + v0*r33,...
%     m14==alfaxx*Xom + u0*tz,...
%     m24==alfayy*Yom + v0*tz);
options.MaxFunEvals = 24000;
options.MaxIter =4000;
vetorParametros = fsolve(@PegaParametroSeparados,[-5 -5 -5 -5 -5 -5],options,...
    Cnx,Cny,Ctx, Cty, Cbx, Cby,m11,m12,m13,m14,m21,m22,m23,m24,r31,r32,r33,tz);
    alfax = vetorParametros(1);
    alfay =vetorParametros(2);
    U0 = vetorParametros(3);
    V0 = vetorParametros(4);
    Xom = vetorParametros(5);
    Yom = vetorParametros(6);
% alfax=Salfaxx;
% alfay=Salfayy;
% U0=Su0;
% V0=Sv0;
% Xom=SXom;
% Yom=SYom;

    



