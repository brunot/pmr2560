function [X, Y, Z ] = To3D(numberOfPoints,u1,v1,u2,v2,kx1,kx2,alfax1,alfax2,u0,v0 ,b )


f1=alfax1/kx1;
%f2=alfax2/kx2;
i=numberOfPoints;
while i >= 1
    
%Transforma em coordenadas da camera    
    x1c(i)=(u1(i)-u0)/kx1;
    y1c(i)=(v1(i)-v0)/kx1;
    x2c(i)=(u2(i)-u0)/kx2;
    y2c(i)=(v2(i)-v0)/kx2;
%Transforma em coordenadas do mundo
    Z(i) = f1*b/(x1c(i)-x2c(i));
    X(i)= -Z(i)*x1c(i)/f1;
    Y(i)= -Z(i)*y1c(i)/f1;
    
    i=i-1;
    
end


end

