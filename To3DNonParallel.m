function [X, Y, Z ] = To3DNonParallel(numberOfPoints,u1,v1,u2,v2, P1, P2 )

i=numberOfPoints;
to3Dmatrix = [P1 zeros(size(P1)); zeros(size(P2)) P2];
to3Dmatrix = pinv(to3Dmatrix);
while i >= 1
        
    A = [u1(i); v1(i); 1; u2(i); v2(i); 1];

    B = to3Dmatrix*A;

    w1 = 1/B(4);
    w2 = 1/B(8);
    X(i) = B(1)/w1;
    Y(i) = B(2)/w1;
    Z(i) = B(3)/w1;
    
    i=i-1;
    
end


end

