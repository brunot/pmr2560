
dx= 32.5;
dy=32.5;
dz=32.5;

u1A = 1352.0;
v1A = 1505.0;
Xs1A = 4.0*dx;
Ys1A = 0.0*dy;
Zs1A = 5.0*dz;

u2A = 1559.0;
v2A = 1321.0;
Xs2A = 3*dx;
Ys2A = 0.0*dy;
Zs2A = 4*dz;

u3A = 1751.0;
v3A = 1149.0;
Xs3A = 2*dx;
Ys3A = 0.0*dy;
Zs3A = 3*dz;

u4A = 2138.0;
v4A = 1154.0;
Xs4A = 2*dx;
Ys4A = 0.0*dy;
Zs4A = 1*dz;

u5A = 2856.0;
v5A = 889.0;
Xs5A = 4.0*dx;
Ys5A= 5*dy;
Zs5A = 0*dz;

u6A = 2170.0;
v6A = 833.0;
Xs6A = 3*dx;
Ys6A = 4*dy;
Zs6A = 0*dz;

u7A = 2526.0;
v7A = 790.0;
Xs7A = 2*dx;
Ys7A = 3*dy;
Zs7A = 0*dz;

u8A = 2379.0;
v8A = 1046.0;
Xs8A = 2*dx;
Ys8A = 1*dy;
Zs8A = 0*dz;

u9A = 1366.0;
v9A = 219.0;
Xs9A = 0.0*dx;
Ys9A = 4*dy;
Zs9A = 5*dz;

u10A = 1585.0;
v10A = 404.0;
Xs10A = 0*dx;
Ys10A = 3*dy;
Zs10A = 4*dz;

u11A = 1775.0;
v11A= 566.0;
Xs11A= 0*dx;
Ys11A= 2*dy;
Zs11A = 3*dz;

u12A= 2181.0;
v12A= 580.0;
Xs12A = 0*dx;
Ys12A = 2*dy;
Zs12A = 1*dz;

u13A = 1149.0;
v13A = 542.0;
Xs13A = 0*dx;
Ys13A = 2*dy;
Zs13A = 6*dz;

u14A = 2371.0;
v14A = 69.0;
Xs14A = 0*dx;
Ys14A = 5*dy;
Zs14A = 1*dz;

u15A = 1352.0;
v15A = 1704.0;
Xs15A = 5*dx;
Ys15A = 0*dy;
Zs15A = 5*dz;

u16A = 1153.0;
v16A= 1140.0;
Xs16A = 2*dx;
Ys16A = 0*dy;
Zs16A = 6*dz;

u17A = 2507.0;
v17A = 1621.0;
Xs17A = 5*dx;
Ys17A = 1*dy;
Zs17A = 0*dz;

u18A = 3001.0;
v18A = 695.0;
Xs18A = 4*dx;
Ys18A = 6*dy;
Zs18A = 0*dz;

u1B = 1395.0;
v1B = 2044.0;
Xs1B = 4.0*dx;
Ys1B = 0.0*dy;
Zs1B = 5.0*dz;

u2B = 1602.0;
v2B = 1836.0;
Xs2B = 3*dx;
Ys2B = 0.0*dy;
Zs2B = 4*dz;

u3B = 1792.0;
v3B = 1644.0;
Xs3B = 2*dx;
Ys3B = 0.0*dy;
Zs3B = 3*dz;

u4B = 2180.0;
v4B = 1644.0;
Xs4B = 2*dx;
Ys4B = 0.0*dy;
Zs4B = 1*dz;

u5B = 2904.0;
v5B = 1604.0;
Xs5B = 4.0*dx;
Ys5B= 5*dy;
Zs5B = 0*dz;

u6B = 2714.0;
v6B = 1469.0;
Xs6B = 3*dx;
Ys6B = 4*dy;
Zs6B = 0*dz;

u7B = 2568.0;
v7B = 1363.0;
Xs7B = 2*dx;
Ys7B = 3*dy;
Zs7B = 0*dz;

u8B = 2422.0;
v8B = 1558.0;
Xs8B = 2*dx;
Ys8B = 1*dy;
Zs8B = 0*dz;

u9B = 1408.0;
v9B = 813.0;
Xs9B = 0.0*dx;
Ys9B = 4*dy;
Zs9B = 5*dz;

u10B = 1625.0;
v10B = 955.0;
Xs10B = 0*dx;
Ys10B = 3*dy;
Zs10B = 4*dz;

u11B = 1814.0;
v11B= 1082.0;
Xs11B= 0*dx;
Ys11B= 2*dy;
Zs11B = 3*dz;

u12B= 2218.0;
v12B= 1090.0;
Xs12B = 0*dx;
Ys12B = 2*dy;
Zs12B = 1*dz;

u13B = 1192.0;
v13B = 1070.0;
Xs13B = 0*dx;
Ys13B = 2*dy;
Zs13B = 6*dz;

u14B = 2402.0;
v14B = 686.0;
Xs14B = 0*dx;
Ys14B = 5*dy;
Zs14B = 1*dz;

u15B = 1395.0;
v15B = 2268.0;
Xs15B = 5*dx;
Ys15B = 0*dy;
Zs15B = 5*dz;

u16B = 1195.0;
v16B= 1645.0;
Xs16B = 2*dx;
Ys16B = 0*dy;
Zs16B = 6*dz;

u17B = 2562.0;
v17B = 2198.0;
Xs17B = 5*dx;
Ys17B = 1*dy;
Zs17B = 0*dz;

u18B = 3047.0;
v18B = 1469.0;
Xs18B = 4*dx;
Ys18B = 6*dy;
Zs18B = 0*dz;


%calibracao Camera A
[tzA ,r31A ,r32A ,r33A ,m11A ,m12A ,m13A ,m14A ,m21A ,m22A ,m23A ,m24A ,alfaxA,alfayA,u0A,v0A,ecA] = CalibraUmaCamera(Xs1A ,Ys1A,u1A,v1A,...
   Xs2A ,Ys2A,u2A,v2A,...
   Xs3A ,Ys3A,u3A,v3A,...
   Xs4A,Ys4A,u4A,v4A,...
   Xs5A ,Ys5A,u5A,v5A,...
   Xs6A ,Ys6A,u6A,v6A,...
   Xs7A ,Ys7A,u7A,v7A,...
   Xs8A ,Ys8A,u8A,v8A,...
   Xs9A ,Ys9A,u9A,v9A,...
   Xs10A ,Ys10A,u10A,v10A,...
   Xs11A,Ys11A,u11A,v11A,...
   Xs12A ,Ys12A,u12A,v12A,...
   Xs13A,Ys13A,u13A,v13A,...
   Xs14A ,Ys14A,u14A,v14A,...
   Xs15A ,Ys15A,u15A,v15A,...
   Xs16A ,Ys16A,u16A,v16A,...
   Xs17A ,Ys17A,u17A,v17A,...
   Xs18A ,Ys18A,u18A,v18A,...
   Zs1A,Zs2A,Zs3A,Zs4A,Zs5A,...
   Zs6A,Zs7A,Zs8A,Zs9A,Zs10A,...
   Zs11A,Zs12A,Zs13A,Zs14A,Zs15A,...
   Zs16A,Zs17A,Zs18A...
   );
%calibracao Camera B
[tzB ,r31B ,r32B ,r33B ,m11B ,m12B ,m13B ,m14B ,m21B ,m22B ,m23B ,m24B ,alfaxB,alfayB,u0B,v0B,ecB] = CalibraUmaCamera(Xs1B ,Ys1B,u1B,v1B,...
   Xs2B ,Ys2B,u2B,v2B,...
   Xs3B ,Ys3B,u3B,v3B,...
   Xs4B,Ys4B,u4B,v4B,...
   Xs5B ,Ys5B,u5B,v5B,...
   Xs6B ,Ys6B,u6B,v6B,...
   Xs7B ,Ys7B,u7B,v7B,...
   Xs8B ,Ys8B,u8B,v8B,...
   Xs9B ,Ys9B,u9B,v9B,...
   Xs10B ,Ys10B,u10B,v10B,...
   Xs11B,Ys11B,u11B,v11B,...
   Xs12B ,Ys12B,u12B,v12B,...
   Xs13B,Ys13B,u13B,v13B,...
   Xs14B ,Ys14B,u14B,v14B,...
   Xs15B ,Ys15B,u15B,v15B,...
   Xs16B ,Ys16B,u16B,v16B,...
   Xs17B ,Ys17B,u17B,v17B,...
   Xs18B ,Ys18B,u18B,v18B,...
   Zs1B,Zs2B,Zs3B,Zs4B,Zs5B,...
   Zs6B,Zs7B,Zs8B,Zs9B,Zs10B,...
   Zs11B,Zs12B,Zs13B,Zs14B,Zs15B,...
   Zs16B,Zs17B,Zs18B...
   );
    